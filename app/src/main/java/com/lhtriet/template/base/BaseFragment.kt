@file:Suppress("MemberVisibilityCanBePrivate")

package com.lhtriet.template.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

import androidx.navigation.fragment.findNavController
import com.flowbirdhub.apps.base.BaseViewModel
import com.lhtriet.template.feature.main.MainActivity
import com.lhtriet.template.tool.extention.createLoading
import com.lhtriet.template.tool.extention.hideKeyboard
import com.lhtriet.template.tool.extention.observe

abstract class BaseFragment<
        BINDING : ViewDataBinding,
        BUSINESS_HANDLER : BaseViewModel<*>> : Fragment() {
    abstract val contestResId: Int

    protected var binding: BINDING? = null

    private var loadingDialog: Dialog? = null

    private var messageDialog: Dialog? = null

    protected abstract var showToolBar: Boolean

    protected abstract var businessHandler: BUSINESS_HANDLER

    /**
     * We implement event interactor binding in this method,
     * since it make sure all child view are created
     * */
    protected open fun onViewsBound(root: View) {}

    /**
     * We clear child views interactor here to prevent memory leak
     * since it make sure clearing callbacks/listeners opp happen before
     * */
    protected open fun cleanUp() {}

    /**
     * sub-class should connect to Dagger using this method
     * */
    open fun connectInjector() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectInjector()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (binding == null) binding =
            DataBindingUtil.inflate(inflater, contestResId, container, false)
        if (showToolBar) showToolBar() else hideToolBar()
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycle.addObserver(businessHandler)
        onViewsBound(view)
        businessHandler.run {
            observe(loading, ::onLoadingStateChanged)
            observe(kickUserOut, ::goKickUser)
            observe(remoteServiceUnavailable, ::onRemoteServiceUnavailable)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadingDialog = createLoading()
    }

    override fun onDestroyView() {
        cleanUp()
        binding = null
        messageDialog = null
        businessHandler.resetState()
        businessHandler.resetObserver(this)
        super.onDestroyView()
    }

    override fun onDetach() {
        // make sure no key board left standing when this fragment is detached
        binding?.root?.windowToken?.run { activity?.hideKeyboard(this@run) }
        super.onDetach()
    }


    private fun goKickUser(shouldKick: Boolean) {
        if (shouldKick) if (activity is MainActivity) (activity as MainActivity).kickUserOut()
    }

    private fun onLoadingStateChanged(loading: Boolean) {
        if (loading)
            showLoading()
        else
            hideLoading()
    }


    private fun onRemoteServiceUnavailable(errorDetails: Pair<String, String>) {
        messageDialog?.dismiss()
        loadingDialog?.dismiss()
    }


    fun setScreenBrightness(maximize: Boolean = false) {
        activity?.run {
            val lp = window.attributes
            lp.screenBrightness = if (maximize)
                WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL
            else
                WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE
            window.attributes = lp
        }
    }

    fun popCurrent() {
        findNavController().popBackStack()
    }

    fun showToolBar() {
        runCatching {
            when (activity) {
                is MainActivity -> (activity as MainActivity).showToolbar()
                is AppCompatActivity -> (activity as AppCompatActivity).supportActionBar?.show()
                else -> activity?.actionBar?.show()
            }
        }.onFailure { e -> e.printStackTrace() }
    }

    fun hideToolBar() {
        runCatching {
            when (activity) {
                is MainActivity -> (activity as MainActivity).hideToolbar()
                is AppCompatActivity -> (activity as AppCompatActivity).supportActionBar?.hide()
                else -> activity?.actionBar?.hide()
            }
        }.onFailure { e -> e.printStackTrace() }
    }

    fun showLoading() {
        loadingDialog?.run { if (isShowing.not()) show() }
    }

    fun hideLoading() {
        loadingDialog?.run { if (isShowing) dismiss() }
    }

    fun kickUser() {
        when (activity) {
            is MainActivity -> (activity as MainActivity).kickUserOut()
        }
    }
}