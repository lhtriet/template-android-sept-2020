package com.flowbirdhub.apps.base

import com.flowbirdhub.apps.data.remote.responseHandler.Response
import com.flowbirdhub.apps.data.remote.responseHandler.ResponseHandler
import java.lang.Exception

abstract class BaseRepository {

    abstract val responseHandler: ResponseHandler

    protected suspend fun <T: Any> runCatching( block: suspend ()-> T): Response<T> {
        return try {
            responseHandler.handleSuccess(block.invoke())
        }catch (e: Exception){
            responseHandler.handleException(e)
        }
    }
}