package com.flowbirdhub.apps.base

import androidx.lifecycle.*
import com.flowbirdhub.apps.data.remote.responseHandler.Response
import com.lhtriet.template.tool.storage.AppConfigurator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel<INSTRUCTION>(private val configurator: AppConfigurator) : ViewModel(),
    CoroutineScope, LifecycleObserver {
    override val coroutineContext: CoroutineContext = Dispatchers.Main

    private val mInstruction: MutableLiveData<INSTRUCTION?> = MutableLiveData()
    val instruction: LiveData<INSTRUCTION?>
        get() = mInstruction

    private val mLoading: MutableLiveData<Boolean> = MutableLiveData()
    val loading: LiveData<Boolean>
        get() = mLoading

    private val mKickUserOut: MutableLiveData<Boolean> = MutableLiveData()
    val kickUserOut: LiveData<Boolean>
        get() = mKickUserOut

    private val mServiceUnavailable: MutableLiveData<Pair<String, String>> =
        MutableLiveData() // 503 error from server
    val remoteServiceUnavailable: LiveData<Pair<String, String>>
        get() = mServiceUnavailable

    fun dispatchInstruction(state: INSTRUCTION) {
        mInstruction.value = state
    }

    fun execute(
        showLoading: Boolean = true,
        ignoreLoading: Boolean = false,
        block: suspend () -> Response<*>?) {

        launch {
            if (ignoreLoading.not())
                if (showLoading)
                    mLoading.value = true

            // remember what in this run block will happen after the block.invoke()
            block.invoke()?.apply {
                if (isSuccess().not()) {
                    error?.run {
                        when {
                            isUnAuthorized() -> mKickUserOut.value = true
                            isServiceUnavailable() -> TODO()
                            else -> {
                            }
                        }
                    }
                }
            }

            if (ignoreLoading.not())
                mLoading.value = false
        }
    }

    fun resetState() {
        mInstruction.value = null
    }

    fun resetObserver(lifecycleOwner: LifecycleOwner) {
        mInstruction.removeObservers(lifecycleOwner)
    }
}