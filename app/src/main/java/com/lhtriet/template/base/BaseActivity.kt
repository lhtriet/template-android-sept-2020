package com.lhtriet.template.base

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.lhtriet.template.R
import com.lhtriet.template.tool.extention.changeStatusBarColor

abstract class BaseActivity<T : ViewDataBinding> : AppCompatActivity() {
    abstract val contentResId: Int

    protected lateinit var binding: T

    protected var messageDialog: Dialog? = null

    open var statusBarColor: Int = R.color.colorPrimary

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding = DataBindingUtil.setContentView(this, contentResId)
        changeStatusBarColor(statusBarColor)
    }

    override fun onDestroy() {
        messageDialog = null
        super.onDestroy()
    }
}