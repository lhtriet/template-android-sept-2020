package com.lhtriet.template.feature.main

import android.content.Context
import com.flowbirdhub.apps.base.BaseViewModel
import com.lhtriet.template.tool.UserManager
import com.lhtriet.template.tool.storage.AppConfigurator
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val context: Context,
    private val userManager: UserManager,
    private val configurator: AppConfigurator
) : BaseViewModel<MainInstruction>(configurator) {


    fun kickUserOut() {
        userManager.logout(context)
        dispatchInstruction(OpenStartScreen)
    }
}

sealed class MainInstruction
object OpenStartScreen : MainInstruction()