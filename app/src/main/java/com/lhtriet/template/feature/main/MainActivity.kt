package com.lhtriet.template.feature.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lhtriet.template.R
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    fun showToolbar() {
        supportActionBar?.show()
    }

    fun hideToolbar() {
        supportActionBar?.hide()
    }

    fun kickUserOut() {
        viewModel.kickUserOut()
    }
}