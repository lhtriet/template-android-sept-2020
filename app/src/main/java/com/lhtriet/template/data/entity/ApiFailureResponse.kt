package com.lhtriet.template.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * A class describes error responses from back-end
 * */
@Parcelize
data class ApiFailureResponse(
    @SerializedName("message") val message: String,
    @SerializedName("exception") val exception: String?) : Parcelable