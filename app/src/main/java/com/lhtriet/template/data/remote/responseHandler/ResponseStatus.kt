package com.flowbirdhub.apps.data.remote.responseHandler

enum class ResponseStatus { SUCCESS, ERROR }