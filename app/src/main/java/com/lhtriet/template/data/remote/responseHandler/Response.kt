package com.flowbirdhub.apps.data.remote.responseHandler

data class Response <out T>(val status: ResponseStatus, val data: T?, val error: ResponseError?){
    companion object{
        fun <T> success(data: T?= null): Response<T>
                = Response(status = ResponseStatus.SUCCESS, data = data, error = null)

        fun <T> error(error: ResponseError): Response<T>
                = Response(status = ResponseStatus.ERROR, data = null, error = error)
    }

    fun isSuccess() = status == ResponseStatus.SUCCESS
}