package com.flowbirdhub.apps.data.remote.responseHandler

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import retrofit2.http.HTTP
import java.net.HttpURLConnection

@Parcelize
data class ResponseError(val message: String, val code: Int): Parcelable{
    fun isUnAuthorized() = code == HttpURLConnection.HTTP_UNAUTHORIZED
    fun isServiceUnavailable() = code == HttpURLConnection.HTTP_UNAVAILABLE
}