package com.flowbirdhub.apps.data.remote.responseHandler

import com.google.gson.Gson
import com.lhtriet.template.data.entity.ApiFailureResponse
import retrofit2.HttpException
import java.net.SocketTimeoutException
import javax.inject.Inject
import kotlin.Exception

/**
 * Credit goes to
 * https://github.com/android/architecture-components-samples/blob/master/GithubBrowserSample/app/src/main/java/com/android/example/github/vo/Resource.kt
 * */
open class ResponseHandler @Inject constructor(private val gson: Gson) {
    fun <T : Any> handleSuccess(data: T): Response<T> {
        if (data is retrofit2.Response<*>) {
            if (data.code() in 400..511) {
                if (data.errorBody() != null) {
                    val error =
                        gson.fromJson(data.errorBody()!!.string(), ApiFailureResponse::class.java)
                    return Response.error(error = ResponseError(error.message, data.code()))
                }
            }
        }
        return Response.success(data)
//        return Response.error(error = ResponseError(message = "Fake 503", code = 503))
    }

    fun <T : Any> handleException(e: Exception): Response<T> {
        e.printStackTrace()
        return when (e) {
            is HttpException -> {
                val code = e.code()
                val mess = gson.fromJson(
                    e.response()?.errorBody()?.string() ?: "",
                    ResponseError::class.java
                )?.message
                if (mess != null)
                    Response.error(error = ResponseError(mess, code))
                else
                    Response.error(error = ResponseError("Can not retrieve error message from API response",
                        code))
            }
            is IllegalStateException ->
                Response.error(error = ResponseError("Response syntax error", ERR_RESPONSE_SYNTAX))
            is SocketTimeoutException ->
                Response.error(error = ResponseError("Timeout", ERR_TIME_OUT))
            else ->
                Response.error(error = ResponseError("Something went wrong", ERR_UNKNOWN))
        }
    }

    companion object {
        const val ERR_TIME_OUT = 999
        const val ERR_UNKNOWN = 666
        const val ERR_SUCCESS_BUT_NULL_RESPONSE = 777
        const val ERR_RESPONSE_SYNTAX = 333
    }
}