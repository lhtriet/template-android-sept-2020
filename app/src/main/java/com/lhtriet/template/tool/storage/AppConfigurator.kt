package com.lhtriet.template.tool.storage

import android.content.Context
import android.content.SharedPreferences
import com.lhtriet.template.BuildConfig
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppConfigurator @Inject constructor(private val context: Context) :
    Storage {
    companion object {
        const val STORAGE_DEVICE_TOKEN = "storage_device_token"
    }

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    override fun setString(key: String, value: String) {
        with(sharedPreferences.edit()) {
            putString(key, value)
            apply()
        }
    }

    override fun getString(key: String): String = sharedPreferences.getString(key, "")!!

    override fun setBoolean(key: String, value: Boolean) {
        with(sharedPreferences.edit()) {
            putBoolean(key, value)
            apply()
        }
    }

    override fun getBoolean(key: String): Boolean = sharedPreferences.getBoolean(key, false)!!
}