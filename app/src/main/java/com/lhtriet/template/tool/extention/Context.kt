package com.lhtriet.template.tool.extention

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.StateListDrawable
import android.net.ConnectivityManager
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.lhtriet.template.R
import com.lhtriet.template.tool.AppConstants
import java.io.FileNotFoundException

fun Context.permissionGranted(permission: String) =
    ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun Context.internetConnected() =
    (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo != null

fun Context.color(id: Int) = ContextCompat.getColor(this, id)

fun Context.colorStateList(id: Int) = ContextCompat.getColorStateList(this, id)

fun Context.drawable(id: Int): Drawable? = try {
    ContextCompat.getDrawable(this, id)
} catch (e: Exception) {
    e.printStackTrace()
    null
}

fun Context.toast(mess: String) = Toast.makeText(this, mess, Toast.LENGTH_SHORT).show()

@Throws(FileNotFoundException::class)
fun Context.drawable(
    assetsName: String,
    context: Context? = null,
    width: Int? = null,
    height: Int? = null): Drawable? {
    runCatching {
        return Drawable.createFromStream(this.assets.open("$assetsName.png"), null).run {
            if (context != null && width != null && height != null) {
                /**
                 * I have to create a new bitmap drawable since the setBounds method
                 * doesn't work with checkbox button
                 * */
                return@run BitmapDrawable(context.resources,
                    Bitmap.createScaledBitmap(this!!.toBitmap(width,
                        height,
                        Bitmap.Config.ARGB_8888), width, height, true)) as Drawable?
            } else {
                return@run this
            }
        }
    }.onFailure { e ->
        e.printStackTrace()
    }
    return null
}

fun Context.selectorDrawable(
    checkedDrawableKeys: String,
    unCheckedDrawableKeys: String): StateListDrawable? {
    runCatching {
        return StateListDrawable().apply {
            val size = resources.getDimensionPixelSize(R.dimen.common_icon_size)
            setExitFadeDuration(AppConstants.ANIM_DURATION)
            addState(intArrayOf(android.R.attr.state_checked),
                drawable(checkedDrawableKeys, this@runCatching, size, size))
            addState(intArrayOf(), drawable(unCheckedDrawableKeys, this@runCatching, size, size))
        }
    }.onFailure { e -> e.printStackTrace() }
    return null
}


fun Context.getVH(layoutId: Int, parent: ViewGroup): View =
    LayoutInflater.from(this).inflate(layoutId, parent, false)


fun Context.hideKeyboard(binder: IBinder) {
    try {
        (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .run { hideSoftInputFromWindow(binder, 0) }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun Context.readAssetsJson(fileName: String): String? {
    var result: String? = null
    runCatching {
        val inputStream = assets.open("$fileName.json")
        result = inputStream.bufferedReader().use { it.readText() }
    }.onFailure { e -> e.printStackTrace() }
    return result
}