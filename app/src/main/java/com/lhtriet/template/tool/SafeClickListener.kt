package com.lhtriet.template.tool

import android.os.SystemClock
import android.view.View

/**
 * Credit goes to https://medium.com/@simonkarmy2004/solving-android-multiple-clicks-problem-kotlin-b99c06135da0
 * */
class SafeClickListener(
    private val defaultInterval: Int = 150,
    private val onSafeClick: (View?) -> Unit) : View.OnClickListener {
    private var lastTimeClicked: Long = 0L

    override fun onClick(v: View?) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < defaultInterval)
            return
        lastTimeClicked = SystemClock.elapsedRealtime()
        onSafeClick.invoke(v)
    }
}

