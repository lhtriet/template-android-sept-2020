package com.lhtriet.template.tool

import android.content.Context
import com.flowbirdhub.apps.data.remote.responseHandler.Response
import com.lhtriet.template.tool.storage.AppConfigurator
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserManager @Inject constructor(
    private val configurator: AppConfigurator
) {
    fun isUserLoggedIn(): Boolean {
        return true
    }

    suspend fun login(email: String, password: String){}

    fun logout(context: Context) {}
}