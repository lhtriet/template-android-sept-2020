package com.lhtriet.template.tool.extention

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.PopUpToBuilder
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.lhtriet.template.R
import com.lhtriet.template.tool.view.AppTextView
import java.lang.Exception


val appNavOptions = navOptions {
    anim {
        enter = R.anim.slide_in_right
        exit = R.anim.slide_out_left
        popEnter = R.anim.slide_in_left
        popExit = R.anim.slide_out_right
    }
}

@SuppressLint("InflateParams")
fun Fragment.createLoading(): Dialog? {
    return try {
        Dialog(activity!!).apply {
            setCancelable(true)
            setContentView(LayoutInflater.from(activity).inflate(R.layout.dialog_loading, null)
                .apply {
                    this.findViewById<AppTextView>(R.id.tvLoading)
                        .setTextColor(context.color(android.R.color.black))
                })
            window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

fun Fragment.navigateTo(deepLink: Uri) {
    runCatching {
        findNavController().navigate(deepLink, appNavOptions)
    }.onFailure { e -> e.printStackTrace() }
}

fun Fragment.navigateTo(id: Int, args: Bundle? = null) {
    runCatching {
        findNavController().navigate(id, args, appNavOptions)
    }.onFailure { e -> e.printStackTrace() }
}

fun Fragment.navigateTo(directions: NavDirections, popUpToDestination: Int = -1) {
    runCatching {
        val options = if (popUpToDestination != -1) navOptions {
            popUpTo(popUpToDestination) {inclusive = true}
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        } else appNavOptions
        findNavController().navigate(directions, options)
    }.onFailure { e -> e.printStackTrace() }
}

fun Fragment.navigateUp() {
    runCatching {
        findNavController().navigateUp()
    }.onFailure { e -> e.printStackTrace() }
}
