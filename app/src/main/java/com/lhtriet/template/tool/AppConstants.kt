package com.lhtriet.template.tool

class AppConstants {
    companion object {
        const val ACCEPT = "Accept"
        const val APPLICATION_JSON = "application/json"
        const val CONTENT_TYPE = "Content-TransactionType"
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer"
        const val N0_RESOURCE_ID = -1

        const val ANIM_DURATION = 300

        const val ACTIVED_PASSES_REFRESH_TIME = 30000L

        const val DEEP_LINK_QUERY_TOKEN = "token"

        /** test only, value will be changed  when goes production */
        const val CREDIT_CARD_TOKEN = "8ac7a4a06a304583016a463a5ddb303f"

        const val MAX_UPLOAD_STORAGE = 1024L * 1024L * 10 // 10Mb max
        const val MAX_UPLOAD_FILE = 3

        const val PATTERN_DEFAULT_CURRENCY = "$#,###.00"

        const val SPACE = " "
        const val UTC = "UTC"
        const val GMT = "GMT"
        const val DATE_PATTERN_FROM_SERVER_WITH_UTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        const val DATE_PATTERN_FROM_SERVER = "yyyy-MM-dd"
        const val DATE_PATTERN_FROM_SERVER_NO_UTC = "yyyy-MM-dd HH:mm:ss"

        const val DATE_PATTERN_PRESENT_DATE_IN_WEEK = "EEE"
        const val DATE_PATTERN_PRESENT_HOUR_IN_DAY = "HH:mm"
        const val DATE_PATTERN_PRESENT_MM_DD_YYYY = "MM/dd/yyyy"
        const val DATE_PATTERN_PRESENT_MESSAGE = "MMMM dd, yyyy"
        const val DATE_PATTERN_PRESENT_CITATION_CREATED_TIME = "MMM, dd, yyyy - hh:mm a"
        const val DATE_PATTERN_ACTIVATE_TIME = "MMM d, yyyy - HH:mm"
        const val DATE_PATTERN_PRESENT_CITATION_PAYMENT_DUE_DATE = "MM/dd/yyyy"


        const val DATE_PATTERN_PRESENT_TRANSACTION_ITEM_DATE_ONLY = "yyyy-MM-dd"
        const val DATE_PATTERN_PRESENT_DBO = "MM/dd/yy"
        const val DATE_PATTERN_PRESENT_TRANSACTION_TIME = "hh:mm a"

        const val DATE_PATTERN_SAVE_FILE = "yyyyMMdd_HHmmss"

        val REGEX_ONLY_NUMBER = Regex("[0-9]")
        val REGEX_MY_PROFILE_DATE = Regex("(\\d{2}[/, -]\\d{2}[/, -]\\d{4})")

        const val REGEX_STRING_NOT_EMPTY = "^(?!\\s*\$).+"


        const val KEY_ERR_API = "Api call error --->"
        const val QR_CODE_REFRESH_RATE = 3000L // this should be read from B.O in production

        const val DEFAULT_PAGE = 1
        const val DEFAULT_ITEM_PER_PAGE = 10

        /** url for privacy and term*/
        const val URL_POLICY = "http://www.austintexas.gov//page/privacy-policy"
        const val URL_TEMPS = "http://www.austintexas.gov/page/legal-notice"
        const val URL_FAQ = "http://www.austintexas.gov/department/pools-and-swimming"
        const val URL_PARD_TEMPS = "http://www.austintexas.gov/page/legal-notice"
    }
}