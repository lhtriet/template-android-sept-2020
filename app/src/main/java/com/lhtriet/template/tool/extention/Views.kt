package com.lhtriet.template.tool.extention

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.lhtriet.template.tool.SafeClickListener

fun ViewGroup.inflateLayout(layoutResId: Int): View =
    LayoutInflater.from(context).inflate(layoutResId, this)

fun ViewGroup.saveChildrenState(): SparseArray<Parcelable> {
    val sparseArray = SparseArray<Parcelable>()
    for (i in 0 until childCount) {
        getChildAt(i).saveHierarchyState(sparseArray)
    }
    return sparseArray
}

fun ViewGroup.restoreChildrenState(childrenState: SparseArray<Parcelable>) {
    for (i in 0 until childCount) {
        getChildAt(i).restoreHierarchyState(childrenState)
    }
}

fun View.toast(mess: String) {
    Toast.makeText(context, mess, Toast.LENGTH_SHORT).show()
}

fun View.touched(l: (View, MotionEvent) -> Unit) {
    setOnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> l.invoke(v, event)
            MotionEvent.ACTION_UP -> performClick()
        }
        return@setOnTouchListener true
    }
}

fun View.clicked(l: (v: View) -> Unit) {
    setOnClickListener(SafeClickListener { l.invoke(this) })
}

fun View.string(resId: Int): String = context.getString(resId)

fun View.gone() {
    if (this.visibility != View.GONE) this.visibility = View.GONE
}

fun View.show() {
    if (this.visibility != View.VISIBLE) this.visibility = View.VISIBLE
}

fun View.invisible() {
    if (this.visibility != View.INVISIBLE) this.visibility = View.INVISIBLE
}

fun View.color(id: Int) = context.color(id)

@SuppressLint("RestrictedApi")
fun BottomNavigationView.disableShiftMode() {
    val menuView = getChildAt(0) as BottomNavigationMenuView
    try {
        for (i in 0 until menuView.childCount) {
            (menuView.getChildAt(i) as BottomNavigationItemView).apply {
                setShifting(false)
                setChecked(itemData.isChecked)
            }
        }
    } catch (e: NoSuchFieldException) {
        Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
    } catch (e: IllegalAccessException) {
        Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
    }
}

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, 0)
}