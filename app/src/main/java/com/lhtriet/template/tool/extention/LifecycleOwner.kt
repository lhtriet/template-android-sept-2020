package com.lhtriet.template.tool.extention

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/**
 * I help bind live data and its observer together
 * */
fun <T : Any?, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T) -> Unit) {
    val owner = if( this is Fragment ) this else this
    liveData.observe(owner, Observer(body))
}

fun <T : Any?, L : LiveData<T>> LifecycleOwner.removeObservers(liveData: L) = liveData.removeObservers(this)