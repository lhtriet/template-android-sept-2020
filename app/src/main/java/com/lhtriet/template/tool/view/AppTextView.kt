package com.lhtriet.template.tool.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView
import com.lhtriet.template.R

open class AppTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.appTextViewStyle
) : TextView(context, attrs, defStyleAttr) {

    @Suppress("MemberVisibilityCanBePrivate")
    var customFont: String? = null
        set(value) {
            runCatching {
                typeface = Typeface.createFromAsset(context.assets, "fonts/$value")
            }.onSuccess { field = value }.onFailure { e -> e.printStackTrace() }
        }

    init {
        attrs?.let {
            with(context.obtainStyledAttributes(attrs, R.styleable.AppTextView)) {
                customFont = getString(R.styleable.AppTextView_customFont)
                    ?: context.getString(R.string.font_roboto_regular)
                recycle()
            }
        }
    }
}