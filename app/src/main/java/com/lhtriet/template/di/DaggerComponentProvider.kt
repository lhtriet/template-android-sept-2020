package com.lhtriet.template.di

import com.flowbirdhub.apps.di.component.AppComponent


interface DaggerComponentProvider {
    val component: AppComponent
}