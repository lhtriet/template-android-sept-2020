package com.flowbirdhub.apps.di.component.sub

import com.flowbirdhub.apps.di.scope.LoggedUserScope
import com.lhtriet.template.feature.home.HomeFragment
import dagger.Subcomponent

@Subcomponent
@LoggedUserScope // only instantiate this component after user has logged in
interface MainFlowComponent {
    @Subcomponent.Factory
    interface Factory{
        fun create(): MainFlowComponent
    }

    fun inject(frag: HomeFragment)

}