package com.flowbirdhub.apps.di.component.sub

import com.flowbirdhub.apps.di.scope.FragmentScope
import com.lhtriet.template.feature.home.HomeFragment
import dagger.Subcomponent

@Subcomponent
@FragmentScope
interface AuthComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): AuthComponent
    }

    fun inject(frag: HomeFragment)
}