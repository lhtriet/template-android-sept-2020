package com.flowbirdhub.apps.di.module

import com.flowbirdhub.apps.di.component.sub.AuthComponent
import com.flowbirdhub.apps.di.component.sub.LoginComponent
import com.flowbirdhub.apps.di.component.sub.MainFlowComponent
import dagger.Module

@Module(subcomponents = [AuthComponent::class, MainFlowComponent::class, LoginComponent::class])
class AppSubComponent