package com.flowbirdhub.apps.di.component

import android.content.Context
import com.flowbirdhub.apps.di.component.sub.AuthComponent
import com.flowbirdhub.apps.di.component.sub.LoginComponent
import com.flowbirdhub.apps.di.module.AppSubComponent
import com.flowbirdhub.apps.di.module.StorageModule
import com.lhtriet.template.feature.home.HomeFragment
import com.lhtriet.template.feature.main.MainActivity
import com.lhtriet.template.tool.UserManager
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppSubComponent::class, StorageModule::class])
interface AppComponent {
    @Component.Factory
    interface Factory{
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun inject(activity: MainActivity)
    fun inject(frag: HomeFragment)

    fun userManager(): UserManager
    fun authComponent(): AuthComponent.Factory
    fun loginComponent(): LoginComponent.Factory
}