package com.flowbirdhub.apps.di.component.sub

import com.flowbirdhub.apps.di.scope.FragmentScope
import com.lhtriet.template.feature.home.HomeFragment
import dagger.Subcomponent

@Subcomponent
@FragmentScope
interface LoginComponent{
    @Subcomponent.Factory
    interface Factory{
        fun create(): LoginComponent
    }

    fun inject(frag: HomeFragment)
}