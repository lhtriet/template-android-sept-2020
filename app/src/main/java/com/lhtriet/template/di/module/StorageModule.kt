package com.flowbirdhub.apps.di.module

import com.lhtriet.template.tool.storage.AppConfigurator
import com.lhtriet.template.tool.storage.Storage
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class StorageModule {
    @Binds
    abstract fun provideStorage(configurator: AppConfigurator): Storage

    @Singleton
    fun provideArtifact(configurator: AppConfigurator): String? = ""
}